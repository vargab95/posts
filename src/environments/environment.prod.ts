export const environment = {
  production: true,
  apiBaseUrl: "https://jsonplaceholder.typicode.com/",
  apiPostsSubUrl: "posts",
  apiCommentsSubUrl: "/comments",
};
