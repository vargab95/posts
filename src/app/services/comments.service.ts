import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import { Comment } from "../model/comment.model";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class CommentsService {
  constructor(private http: HttpClient) {}

  get(postId: number): Observable<Array<Comment>> {
    return this.http.get<Array<Comment>>(
      `${environment.apiBaseUrl}${environment.apiPostsSubUrl}/${postId}${environment.apiCommentsSubUrl}`
    );
  }
}
