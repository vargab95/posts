import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { environment } from "../../environments/environment";
import { Post } from "../model/post.model";

@Injectable({
  providedIn: "root",
})
export class PostsService {
  constructor(private http: HttpClient) {}

  get(): Observable<Array<Post>> {
    return this.http.get<Array<Post>>(
      environment.apiBaseUrl + environment.apiPostsSubUrl
    );
  }

  add(post: Post): Observable<Post> {
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
      }),
    };

    return this.http.post<Post>(
      environment.apiBaseUrl + environment.apiPostsSubUrl,
      post,
      httpOptions
    );
  }
}
