import { Component, OnInit } from "@angular/core";
import { PostsService } from "./services/posts.service";
import { Post } from "./model/post.model";
import { MatDialog } from "@angular/material/dialog";
import { CreatePostDialog } from "./components/post-dialog/post-dialog.component";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  title = "Posts";
  posts: Array<Post> = [];
  loading: boolean = true;

  constructor(
    private postsService: PostsService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.postsService.get().subscribe((posts) => {
      this.posts = posts;
      this.loading = false;
    });
  }

  onCreatePost(): void {
    const dialogRef = this.dialog.open(CreatePostDialog);

    dialogRef.afterClosed().subscribe(
      (post) => {
        if (!!post) {
          this.postsService.add(post).subscribe((response: Post) => {
            this.posts.push(response);
            this.snackBar.open("Post was created successfully", "", {
              duration: 2000,
            });
          });
        }
      },
      () => {
        this.snackBar.open("There was an error during post creation", "", {
          duration: 2000,
        });
      }
    );
  }
}
