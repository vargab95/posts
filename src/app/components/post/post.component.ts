import { Component, Input } from "@angular/core";

import { Post } from "src/app/model/post.model";
import { Comment } from "src/app/model/comment.model";
import { CommentsService } from "src/app/services/comments.service";

@Component({
  selector: "app-post",
  templateUrl: "./post.component.html",
  styleUrls: ["./post.component.css"],
})
export class PostComponent {
  @Input() post: Post;
  loading: boolean = false;
  comments: Array<Comment> = null;

  constructor(private commentsService: CommentsService) {}

  onShowComments() {
    this.loading = true;
    this.commentsService.get(this.post.id).subscribe((comments) => {
      this.comments = comments;
      this.loading = false;
    });
  }

  onHideComments() {
    this.comments = [];
  }
}
