import { Component, Input } from "@angular/core";

import { Comment } from "src/app/model/comment.model";

@Component({
  selector: "app-comment",
  templateUrl: "./comment.component.html",
  styleUrls: ["./comment.component.css"],
})
export class CommentComponent {
  @Input() comment: Comment;

  constructor() {}
}
