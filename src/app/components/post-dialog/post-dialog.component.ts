import { Component, OnInit } from "@angular/core";
import { MatDialogRef } from "@angular/material/dialog";
import { Post } from "src/app/model/post.model";
import { FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: "app-post-dialog",
  templateUrl: "./post-dialog.component.html",
  styleUrls: ["./post-dialog.component.css"],
})
export class CreatePostDialog implements OnInit {
  formGroup: FormGroup;

  constructor(public dialogRef: MatDialogRef<CreatePostDialog>) {}

  onCancel(): void {
    this.dialogRef.close(null);
  }

  onCreate(): void {
    this.dialogRef.close({
      title: this.formGroup.value.title,
      body: this.formGroup.value.body,
    } as Post);
  }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      title: new FormControl(null, Validators.required),
      body: new FormControl(null, Validators.required),
    });
  }
}
